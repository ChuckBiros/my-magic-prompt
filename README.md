# My Magic Prompt

My Magic Prompt est un prompt interactif en ligne de commande développé en Bash, offrant diverses fonctionnalités pour la gestion des fichiers, l'interaction utilisateur et d'autres fonctionnalités utiles.

## Fonctionnalités

- Affichage de l'aide (`help`)
- Listage des fichiers et dossiers, y compris les fichiers cachés (`ls`)
- Suppression de fichiers (`rm`)
- Suppression de dossiers (`rmd` ou `rmdir`)
- Affichage des informations sur le programme (`about`)
- Affichage de la version du prompt (`version`)
- Vérification de l'âge de l'utilisateur (`age`)
- Sortie du prompt (`quit`)
- Affichage du profil utilisateur (`profil`)
- Changement de mot de passe avec confirmation (`passw`)
- Changement de répertoire (`cd`)
- Affichage du répertoire courant (`pwd`)
- Affichage de l'heure actuelle (`hour`)
- Téléchargement du code source HTML d'une page web (`httpget`)
- Envoi d'e-mails (`smtp`)
- Ouverture de fichiers dans l'éditeur Vim (`open`)
- Jouer à Rock Paper Scissors (`rps`)
- Supprimer un ou plusieurs dossiers avec un mot de passe (`rmdirwtf`)

## Utilisation

Pour utiliser My Magic Prompt, exécutez le script `main.sh` situé dans le répertoire `~/my-magic-prompt/` :

```bash
~/my-magic-prompt/main.sh
```

Une fois le prompt lancé, vous pouvez saisir les commandes disponibles. Pour obtenir la liste des commandes disponibles, tapez `help`.

## Exemples

- Pour jouer à Rock Paper Scissors :
  ```bash
  rps
  ```

- Pour supprimer un dossier avec mot de passe :
  ```bash
  rmdirwtf
  ```

## Remarques

- Pour quitter le prompt, utilisez la commande `quit`.
- Pour plus de détails sur chaque commande, vous pouvez utiliser la commande `help`.

## Auteur

Ce prompt a été développé par Boris Montron.