#!/bin/bash

# Variables
password="password"
login="user"
current_dir="~"
p1=""
p2=""
p1_score=0
p2_score=0

# Fonction help
function help {
    echo "Commandes disponibles :"
    echo "help : affiche les commandes disponibles"
    echo "ls : lister les fichiers et dossiers (y compris les fichiers cachés)"
    echo "rm <file> : supprimer un fichier"
    echo "rmd ou rmdir <directory> : supprimer un dossier"
    echo "about : affiche une description du programme"
    echo "version ou --v ou vers : affiche la version du prompt"
    echo "age : demande votre âge et vous dit si vous êtes majeur ou mineur"
    echo "quit : sortir du prompt"
    echo "profil : afficher toutes les informations sur vous-même"
    echo "passw : changer le mot de passe avec demande de confirmation"
    echo "cd <directory> : aller dans un dossier ou revenir au dossier précédent"
    echo "pwd : affiche le répertoire actuel"
    echo "hour : affiche l'heure actuelle"
    echo "httpget : télécharge le code source html d'une page web et l'enregistre dans un fichier spécifié"
    echo "smtp : envoie un email avec une adresse, un sujet et le contenu du mail"
    echo "open <file> : ouvre un fichier dans l'éditeur VIM"
    echo "rps : jouer à Rock Paper Scissors"
    echo "rmdirwtf : supprimer un ou plusieurs dossiers (mot de passe requis)"
}

# Fonction ls
function list_files {
    ls -a "$current_dir"
}

# Fonction rm
function remove_file {
    rm "$1"
}

# Fonction rmd/rmdir
function remove_directory {
    rmdir "$1"
}

# Fonction about
function about {
    echo "Ce prompt bash est un programme interactif qui permet d'exécuter diverses commandes."
}

# Fonction version
function version {
    echo "Version 2.0"
}

# Fonction age
function check_age {
    read -p "Quel est votre âge ? " age
    if ((age >= 18)); then
        echo "Vous êtes majeur."
    else
        echo "Vous êtes mineur."
    fi
}

# Fonction quit
function quit {
    echo "Au revoir !"
    exit
}

# Fonction profil
function profil {
    echo "Prénom: John"
    echo "Nom: Doe"
    echo "Âge: 30"
    echo "Email: john.doe@example.com"
}

# Fonction passw
function change_password {
    read -s -p "Entrez votre ancien mot de passe : " old_password
    echo ""
    if [[ $old_password == $password ]]; then
        read -s -p "Entrez votre nouveau mot de passe : " new_password
        echo ""
        read -s -p "Confirmez votre nouveau mot de passe : " confirm_password
        echo ""
        if [[ $new_password == $confirm_password ]]; then
            password=$new_password
            echo "Mot de passe changé avec succès."
        else
            echo "Les mots de passe ne correspondent pas."
        fi
    else
        echo "Mot de passe incorrect."
    fi
}

# Fonction cd
function change_directory {
    if [[ "$1" == ".." ]]; then
        cd ..
    else
        cd "$1"
    fi
    current_dir=$(pwd)
}

# Fonction pwd
function print_current_dir {
    echo "Répertoire actuel : $current_dir"
}

# Fonction hour
function print_current_hour {
    echo "Heure actuelle : $(date +"%T")"
}

# Fonction httpget
function httpget {
    read -p "URL de la page web : " url
    read -p "Nom du fichier de sauvegarde : " filename
    curl -s "$url" -o "$filename"
    echo "Téléchargement terminé."
}

# Fonction smtp
function smtp {
    read -p "Adresse email : " address
    read -p "Sujet : " subject
    read -p "Corps du mail : " body
    echo "$body" | mail -s "$subject" "$address"
    echo "Email envoyé avec succès."
}

# Fonction open
function open_file {
    vim "$1"
}

# Fonction rps
function rock_paper_scissors {
    echo "Bienvenue au jeu Rock Paper Scissors !"
    read -p "Entrez le nom du joueur 1 : " p1
    read -p "Entrez le nom du joueur 2 : " p2
    echo "Le jeu commence !"
    
    while true; do
        echo "$p1, c'est votre tour :"
        read -p "Entrez votre choix (rock, paper, scissors) ou SuperKitty : " choice1
        echo "$p2, c'est votre tour :"
        read -p "Entrez votre choix (rock, paper, scissors) ou SuperKitty : " choice2
        
        if [[ $choice1 == "SuperKitty" ]]; then
            echo "$p1 a utilisé le pouvoir ultime ! $p1 remporte la manche !"
            ((p1_score++))
        elif [[ $choice2 == "SuperKitty" ]]; then
            echo "$p2 a utilisé le pouvoir ultime ! $p2 remporte la manche !"
            ((p2_score++))
        elif [[ $choice1 == $choice2 ]]; then
            echo "Égalité !"
        elif [[ $choice1 == "rock" && $choice2 == "scissors" || $choice1 == "paper" && $choice2 == "rock" || $choice1 == "scissors" && $choice2 == "paper" ]]; then
            echo "$p1 remporte la manche !"
            ((p1_score++))
        else
            echo "$p2 remporte la manche !"
            ((p2_score++))
        fi
        
        echo "Score : $p1 : $p1_score, $p2 : $p2_score"
        
        if ((p1_score >= 3)); then
            echo "$p1 a remporté la partie !"
            break
        elif ((p2_score >= 3)); then
            echo "$p2 a remporté la partie !"
            break
        fi
    done
}

# Fonction rmdirwtf
function remove_directory_wtf {
    read -s -p "Entrez votre mot de passe : " input_password
    echo ""
    if [[ $input_password == $password ]]; then
        read -p "Entrez le nom du dossier à supprimer : " directory
        rm -r "$directory"
        echo "Dossier '$directory' supprimé avec succès."
    else
        echo
